import itkdb

import dash
from dash import html, dash_table

import pandas as pd

from isi import myitkdb

dash.register_page(__name__, title='Hybrid Test Panels')

layout = html.Div(["This is a page"])


filterMap={'project':'S', 'componentType':'HYBRID_TEST_PANEL'}
data=myitkdb.client.get('listComponents', json={'filterMap':filterMap})
df=pd.json_normalize(data)
df=df.sort_values('serialNumber')

def exrtact_properties(properties):
    properties=pd.DataFrame(properties)
    properties=properties.set_index('code')
    return properties['value']

def count_children(children):
    children=pd.DataFrame(children)
    children=children[['componentType','component']]
    children=children[children['component'].values!=None]
    children=children.groupby('componentType').size()
    return children

#print(df['properties'].apply(exrtact_properties))
df=df.drop(columns=['properties']).merge(df['properties'].apply(exrtact_properties), left_index=True, right_index=True)
df=df.drop(columns=['children']).merge(df['children'].apply(count_children), left_index=True, right_index=True)
df=df[df['state']=='ready']

#
# Create independent tables for institutes
useful_columns=['serialNumber','alternativeIdentifier','LOCAL_NAME','type.name','cts','currentStage.name','NO_CYCLES','600175bae4caf8000a50ce77','trashed','completed','shipmentDestination.code','shipmentDestination.id']
tables=[]
df=df.sort_values('cts')
for currentLocation, test_panels_institute in df.groupby('currentLocation.name'):
    tables.append(html.H2(children=currentLocation))
    tables.append(dash_table.DataTable(data=test_panels_institute[useful_columns].to_dict('records')))

#
# Save the layout
layout = html.Div(tables)