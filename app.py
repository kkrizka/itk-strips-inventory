import dash
from dash import Dash #, dcc, html, dash_table

import itkdb

import importlib

from isi import config
from isi import myitkdb
from isi import template

#
# Prepare the database
myitkdb.initialize(config.accessCode1, config.accessCode2)

#
# Creat the dash app
app = Dash(__name__, title='ITk Strips Inventory', use_pages=True)

#
# Generate the layout
dash.page_container.id='_pages_content'

templates_path=importlib.resources.files('isi')
app.layout = template.jinja2_to_dash(templates_path.joinpath('templates'),
                                        "index.html", replace=[dash.page_container])

#
# RUN ME
if __name__ == '__main__':
    app.run(debug=True)
