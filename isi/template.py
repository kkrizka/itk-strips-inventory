import jinja2

from dash import dcc
from dash import html

import xml.etree.ElementTree as ET

def et_to_dash(etroot, replace=[]):
    dashels=[]

    # Add text to first child
    if etroot.text:
        text=etroot.text.strip()
        if text: dashels.append(text)

    # Add children, each followed by its tail text
    for child in etroot:
        tag=child.tag.split('}')[-1]

        # Check if child is part of replace
        myid=child.attrib.get('id',None)
        dashel=next(filter(lambda el: el.id==myid, replace),None)

        #
        # Need to create child
        if dashel is None:
            childels=et_to_dash(child, replace)

            # Get attributes, some need special parsin
            attrib=child.attrib
            if 'style' in attrib: # has to be a dict
                attrib['style']={line.split(':')[0].strip():line.split(':')[1].strip() for line in attrib['style'].split(';')}
            if 'class' in attrib: # Not class, but className
                attrib['className']=attrib.pop('class')

            # Create the object
            dashel=None
            if tag.startswith('dcc.'):
                # convert attribs types if value is prefix with `type:`
                conversions={
                    'bool':lambda x: x.lower() in ['true','1','yes'],
                    'int':int
                    }
                for k,v in attrib.items():
                    parts=v.split(':')
                    if len(parts)<2: continue # not this
                    v=':'.join(parts[1:])
                    tname=parts[0]
                    if tname in conversions: # know how to convert
                        attrib[k]=conversions[tname](v)

                # create
                dashel=getattr(dcc,tag.split('.')[-1])(**attrib, children=childels)
            else:
                dashel=getattr(html,tag.capitalize())(childels, **attrib)
        dashels.append(dashel)

        # Text after child
        if child.tail:
            text=child.tail.strip()
            if text: dashels.append(text)

    return dashels
    

def html_to_dash(htmlstr, replace=[]):
    # Parse HTML string
    htmldom=ET.fromstring(htmlstr)
    
    # Get body
    body=htmldom.find('{http://www.w3.org/1999/xhtml}body')

    # Add child of body to Dash's root <div/>
    dashels=et_to_dash(body, replace)
    dashrootdiv=html.Div(dashels)        

    return dashrootdiv

def jinja2_to_dash(path='webreport/template', page='index.html', replace=[], **kwargs):
    file_loader = jinja2.FileSystemLoader(path)
    env = jinja2.Environment(loader=file_loader)

    t=env.get_template(page)
    output=t.render(**kwargs)

    return html_to_dash(output, replace)
