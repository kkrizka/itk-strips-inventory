import itkdb

client = None

def initialize(accessCode1, accessCode2):
    global client

    user = itkdb.core.User(access_code1=accessCode1, access_code2=accessCode2, save_auth='.auth')
    client = itkdb.Client(user=user)
    client.user.authenticate()
